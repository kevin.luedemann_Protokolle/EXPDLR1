reset
set terminal epslatex color
set output 'WasserSandSolar.tex'

set xlabel "Zeit [s]"
set ylabel "T [$\\si{\\celsius}$]"

set samples 2000
set key bottom left

set fit logfile 'WasserSandSolar.fit'
set fit errorvariables 

f1(x) = a1*x+b1
f2(x) = a2*x+b2

fit f1(x) "WasserSandSolar.dat" u 1:2:(0.5) via a1,b1
fit f2(x) "WasserSandSolar.dat" u 1:3:(0.5) via a2,b2

p "WasserSandSolar.dat" u 1:2:(0.5) w ye t'Wasser',\
  "WasserSandSolar.dat" u 1:3:(0.5) w ye t'Sand',\
  f1(x) lt -1 t sprintf("Wasser %.03f(%.03f) x %+.03f(%.03f)", a1,a1_err,b1,b1_err),\
  f2(x) lt 0 t sprintf("Sand %.03f(%.03f) x %+.03f(%.03f)", a2,a2_err,b2,b2_err)

set output
!epstopdf WasserSandSolar.eps
!rm WasserSandSolar.eps
