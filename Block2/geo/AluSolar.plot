reset
set terminal epslatex color
set output 'AluSolar.tex'

set xlabel "Zeit [s]"
set ylabel "T [$\\si{\\celsius}$]"

set samples 2000
set key bottom left

set fit logfile 'AluSolar.fit'
set fit errorvariables 

f1(x) = a1*x+b1
f2(x) = a2*x+b2

fit f1(x) "AluSolar.dat" u 1:2:(0.5) via a1,b1
fit f2(x) "AluSolar.dat" u 1:3:(0.5) via a2,b2

p "AluSolar.dat" u 1:2:(0.5) w ye t'dunkel',\
  "AluSolar.dat" u 1:3:(0.5) w ye t'hell',\
  f1(x) lt -1 t sprintf("dunkel %.02f(%.02f) x %+.02f(%.02f)", a1,a1_err,b1,b1_err),\
  f2(x) lt 0 t sprintf("hell %.03f(%.03f) x %+.03f(%.03f)", a2,a2_err,b2,b2_err)

set output
!epstopdf AluSolar.eps
!rm AluSolar.eps
